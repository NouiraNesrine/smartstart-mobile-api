/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entite;

//import java.sql.Date;
//import java.util.Calendar;




/**
 *
 * @author NOUIRA Nesrine
 */
public class Projet {
    
    
  private int  idProjet;
  private int  idProprietaire;
  private int typeProprietaire;
  private String libelleProjet;
  private String descProjet;
  private String cptProjet1;
  private String cptProjet2;
  private String cptProjet3;
  private String cptProjet4;
  private String cptProjet5;
  private double bdgMinProjet;
  private double bdgMaxProjet;
  private int periodeAffProjet;
  //private Date datePostProjet;
  //private Date dateSupProjet;
  private int nbProposition;

   public Projet(){
        }
   
   public Projet(int  idProjet,int idProprietaire, int typeProprietaire, String libelleProjet, String descProjet, String cptProjet1, String cptProjet2, String cptProjet3, String cptProjet4, String cptProjet5, double bdgMinProjet, double bdgMaxProjet, int periodeAffProjet) {
        this.idProjet = idProjet;
        this.idProprietaire = idProprietaire;
        this.typeProprietaire = typeProprietaire;
        this.libelleProjet = libelleProjet;
        this.descProjet = descProjet;
        this.cptProjet1 = cptProjet1;
        this.cptProjet2 = cptProjet2;
        this.cptProjet3 = cptProjet3;
        this.cptProjet4 = cptProjet4;
        this.cptProjet5 = cptProjet5;
        this.bdgMinProjet = bdgMinProjet;
        this.bdgMaxProjet = bdgMaxProjet;
        this.periodeAffProjet = periodeAffProjet;
       // this.datePostProjet = getDate();
       // this.dateSupProjet=setDate(datePostProjet,periodeAffProjet);
        this.nbProposition = 0;
    }
    public Projet(int idProprietaire, int typeProprietaire, String libelleProjet, String descProjet, String cptProjet1, String cptProjet2, String cptProjet3, String cptProjet4, String cptProjet5, double bdgMinProjet, double bdgMaxProjet, int periodeAffProjet) {
        this.idProprietaire = idProprietaire;
        this.typeProprietaire = typeProprietaire;
        this.libelleProjet = libelleProjet;
        this.descProjet = descProjet;
        this.cptProjet1 = cptProjet1;
        this.cptProjet2 = cptProjet2;
        this.cptProjet3 = cptProjet3;
        this.cptProjet4 = cptProjet4;
        this.cptProjet5 = cptProjet5;
        this.bdgMinProjet = bdgMinProjet;
        this.bdgMaxProjet = bdgMaxProjet;
        this.periodeAffProjet = periodeAffProjet;
       // this.datePostProjet = getDate();
        //this.dateSupProjet=setDate(datePostProjet,periodeAffProjet);
        this.nbProposition = 0;
    }
   

   // public Date getDateSupProjet() {
   //     return dateSupProjet;
   // }

   // public void setDateSupProjet(Date dateSupProjet) {
   //     this.dateSupProjet = dateSupProjet;
   // }

    public int getIdProjet() {
        return idProjet;
    }

    public void setIdProjet(int idProjet) {
        this.idProjet = idProjet;
    }

    public int getIdProprietaire() {
        return idProprietaire;
    }

    public void setIdProprietaire(int idProprietaire) {
        this.idProprietaire = idProprietaire;
    }

    public int getTypeProprietaire() {
        return typeProprietaire;
    }

    public void setTypeProprietaire(int typeProprietaire) {
        this.typeProprietaire = typeProprietaire;
    }

    public String getLibelleProjet() {
        return libelleProjet;
    }

    public void setLibelleProjet(String libelleProjet) {
        this.libelleProjet = libelleProjet;
    }

    public String getDescProjet() {
        return descProjet;
    }

    public void setDescProjet(String descProjet) {
        this.descProjet = descProjet;
    }

    public String getCptProjet1() {
        return cptProjet1;
    }

    public void setCptProjet1(String cptProjet1) {
        this.cptProjet1 = cptProjet1;
    }

    public String getCptProjet2() {
        return cptProjet2;
    }

    public void setCptProjet2(String cptProjet2) {
        this.cptProjet2 = cptProjet2;
    }

    public String getCptProjet3() {
        return cptProjet3;
    }

    public void setCptProjet3(String cptProjet3) {
        this.cptProjet3 = cptProjet3;
    }

    public String getCptProjet4() {
        return cptProjet4;
    }

    public void setCptProjet4(String cptProjet4) {
        this.cptProjet4 = cptProjet4;
    }

    public String getCptProjet5() {
        return cptProjet5;
    }

    public void setCptProjet5(String cptProjet5) {
        this.cptProjet5 = cptProjet5;
    }

    public double getBdgMinProjet() {
        return bdgMinProjet;
    }

    public void setBdgMinProjet(double bdgMinProjet) {
        this.bdgMinProjet = bdgMinProjet;
    }

    public double getBdgMaxProjet() {
        return bdgMaxProjet;
    }

    public void setBdgMaxProjet(double bdgMaxProjet) {
        this.bdgMaxProjet = bdgMaxProjet;
    }

    public int getPeriodeAffProjet() {
        return periodeAffProjet;
    }

    public void setPeriodeAffProjet(int periodeAffProjet) {
        this.periodeAffProjet = periodeAffProjet;
    }

  /*  public Date getDatePostProjet() {
        return datePostProjet;
    }

    public void setDatePostProjet(Date datePostProjet) {
        this.datePostProjet = datePostProjet;
    }
*/
    public int getNbProposition() {
        return nbProposition;
    }

    public void setNbProposition(int nbProposition) {
        this.nbProposition = nbProposition;
    }
 
    
  /*  public static java.sql.Date getDate(){
        java.sql.Date sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        return sqlDate;
}
    
    public static java.sql.Date setDate(java.sql.Date date ,int days){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, days);
        java.sql.Date sqlDate = new java.sql.Date(c.getTime().getTime());
        return sqlDate;
}
  */  
 
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Entite;

//import java.sql.Date;

/**
 *
 * @author NOUIRA Nesrine
 */
public class Proposition {
     private int idPProjet;
   private int idProposition;
   private int idPMember;
   private String descProposition;
   private double budgetProposition;
   
   //private Date dateProposition;

   public Proposition(){
   }
    public Proposition(int idPProjet, int idPMember, double budgetProposition) {
        this.idPProjet = idPProjet;
        this.idPMember = idPMember;
        this.budgetProposition = budgetProposition;
    }
    public Proposition(int idPProjet, int idPMember,String descProposition, double budgetProposition) {
        this.idPProjet = idPProjet;
        this.idPMember = idPMember;
        this.descProposition=descProposition;
        this.budgetProposition = budgetProposition;
    }

    public String getDescProposition() {
        return descProposition;
    }

    public void setDescProposition(String descProposition) {
        this.descProposition = descProposition;
    }

    public int getIdPProjet() {
        return idPProjet;
    }
    
    public void setIdPProjet(int idPProjet) {
        this.idPProjet = idPProjet;
    }

    public int getIdProposition() {
        return idProposition;
    }

    public void setIdProposition(int idProposition) {
        this.idProposition = idProposition;
    }

    public int getIdPMember() {
        return idPMember;
    }

    public void setIdPMember(int idPMember) {
        this.idPMember = idPMember;
    }

    public double getBudgetProposition() {
        return budgetProposition;
    }

    public void setBudgetProposition(double budgetProposition) {
        this.budgetProposition = budgetProposition;
    }
   
    /*
    public Date getDateProposition() {
        return dateProposition;
    }
*/
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.mycompany.Entite.Projet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author bhk
 */
public class ServiceProjet {

    ArrayList<Projet> listP = new ArrayList<>();
    public ArrayList<Projet> searchPro(String str) {
        
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        
        String Url = "http://localhost/PiDev/web/app_dev.php/gm/search/"+str;
        con.setUrl(Url);// Insertion de l'URL de notre demande de connexion

        con.addResponseListener((e) -> {
            String st = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println("RESSSSPPPPOOONNNSSEE"+st);//Affichage de la réponse serveur sur la console
            ServiceProjet ser = new ServiceProjet();
            listP = ser.parseListTaskJson(new String(con.getResponseData()));

        });
        NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
    return listP; 
   }
    

    public ArrayList<Projet> parseListTaskJson(String json) {

        ArrayList<Projet> listProjets = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();
            Map<String, Object> tasks = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
            
            for (Map<String, Object> obj : list) {
                
                Projet p = new Projet();
                float id = Float.parseFloat(obj.get("idprojet").toString());
                float idProp = Float.parseFloat(obj.get("idproprietaire").toString());
                float typeproprietaire = Float.parseFloat(obj.get("typeproprietaire").toString());
                float bdgminprojet = Float.parseFloat(obj.get("bdgminprojet").toString());
                float bdgmaxprojet = Float.parseFloat(obj.get("bdgmaxprojet").toString());
                float periodeaffprojet = Float.parseFloat(obj.get("periodeaffprojet").toString());
               
              //  float nbparticipants = Float.parseFloat(obj.get("nbparticipants").toString());

                p.setIdProjet((int) id);
                p.setIdProprietaire((int) idProp);
                p.setTypeProprietaire((int) typeproprietaire);
                p.setLibelleProjet(obj.get("libelleprojet").toString());
                p.setDescProjet(obj.get("descprojet").toString());
                 if (obj.get("cptprojet1")!=null) {
                    p.setCptProjet1(obj.get("cptprojet1").toString());
                }
                  if (obj.get("cptprojet2")!=null) {
                    p.setCptProjet2(obj.get("cptprojet2").toString());
                }
                   if (obj.get("cptprojet3")!=null) {
                    p.setCptProjet3(obj.get("cptprojet3").toString());
                }
                    if (obj.get("cptprojet4")!=null) {
                    p.setCptProjet4(obj.get("cptprojet4").toString());
                }
                if (obj.get("cptprojet5")!=null) {
                    p.setCptProjet5(obj.get("cptprojet5").toString());
                }
               
                p.setBdgMinProjet((double) bdgminprojet);
                p.setBdgMaxProjet((double) bdgmaxprojet);
                p.setPeriodeAffProjet((int) periodeaffprojet);
                 if (obj.get("nbparticipants")!=null) {
                     float nbparticipants = Float.parseFloat(obj.get("nbparticipants").toString());
                    p.setNbProposition((int)nbparticipants);
                }else
                 {
                     p.setNbProposition(0);
                 }
                listProjets.add(p);
            }

        } catch (IOException ex) {
        }
        
        return listProjets;

    }
    
    
    ArrayList<Projet> listProjets = new ArrayList<>();
    
    public ArrayList<Projet> getList2(){       
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PiDev/web/app_dev.php/gm/mobileApiProjetListe");  
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceProjet ser = new ServiceProjet();
                listProjets = ser.parseListTaskJson(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listProjets;
    }
    
    
     public Projet getProjet1(int id){       
        ArrayList<Projet> listPro = new ArrayList<>();
        listPro = getList2();
        for(Projet pro: listPro){
        if(pro.getIdProjet()==id){
         return pro;
        }
        }
       return null;
    }

}

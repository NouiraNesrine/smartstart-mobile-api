/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Service;

import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.mycompany.Entite.Proposition;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author NOUIRA Nesrine
 */
public class ServiceProposition {
    
    public void ajoutTask(Proposition ta) {
        
        ConnectionRequest con = new ConnectionRequest();// création d'une nouvelle demande de connexion
        
        String Url = "http://localhost/PiDev/web/app_dev.php/gm/props/"+ta.getIdPMember()+"/"+ta.getBudgetProposition()+"/"+ta.getDescProposition()+"/"+ta.getIdPProjet();// création de l'URL
        con.setUrl(Url);// Insertion de l'URL de notre demande de connexion

        con.addResponseListener((e) -> {
            String str = new String(con.getResponseData());//Récupération de la réponse du serveur
            System.out.println(str);//Affichage de la réponse serveur sur la console

        });
        NetworkManager.getInstance().addToQueueAndWait(con);// Ajout de notre demande de connexion à la file d'attente du NetworkManager
    }

    public ArrayList<Proposition> parseListTaskJson(String json) {

        ArrayList<Proposition> listProps = new ArrayList<>();

        try {
            JSONParser j = new JSONParser();
            Map<String, Object> tasks = j.parseJSON(new CharArrayReader(json.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) tasks.get("root");
            
            for (Map<String, Object> obj : list) {
                
                Proposition p = new Proposition();
                float id = Float.parseFloat(obj.get("idpprojet").toString());
                float idProp = Float.parseFloat(obj.get("idproposition").toString());
                float proprietaire = Float.parseFloat(obj.get("idpmember").toString());
                float bdgProjet = Float.parseFloat(obj.get("budgetproposition").toString());
               
                p.setIdPProjet((int) id);
                p.setIdProposition((int) idProp);
                p.setIdPMember((int) proprietaire);
                p.setBudgetProposition((double) bdgProjet);
               
                 if (obj.get("descProposition")!=null) {
                    p.setDescProposition(obj.get("descProposition").toString());
                }
                 
                listProps.add(p);
            }

        } catch (IOException ex) {
        }
        
        return listProps;

    }
    
    
    ArrayList<Proposition> listPros= new ArrayList<>();
    
    public ArrayList<Proposition> getListProps(){       
        ConnectionRequest con = new ConnectionRequest();
        con.setUrl("http://localhost/PiDev/web/app_dev.php/gm/mobileApiPropositionListe");  
        con.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                ServiceProposition ser = new ServiceProposition();
                listPros = ser.parseListTaskJson(new String(con.getResponseData()));
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(con);
        return listPros;
    }
    
     public ArrayList<Proposition> getListeProps(int id){       
        ArrayList<Proposition> listVide = new ArrayList<>();
        ArrayList<Proposition> list = getListProps();
        if(list != null){
        for(Proposition pro: list){
        if(pro.getIdPProjet()==id){
         listVide.add(pro);
        }
        } return listVide;}
       return null;
    }
    
}

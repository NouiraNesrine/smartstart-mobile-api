/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui;

import static com.codename1.charts.util.ColorUtil.rgb;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.Slider;
import com.codename1.ui.Stroke;
import com.codename1.ui.TextArea;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.events.DataChangedListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.RoundBorder;
import com.mycompany.Entite.Projet;
import com.mycompany.Entite.Proposition;
import com.mycompany.Service.ServiceProposition;

/**
 *
 * @author NOUIRA Nesrine
 */
public class Postuler {
   Form f;
     
     public Postuler() {
        f = new Form();
       
       
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    } 
    public void formulaireAff(Projet c){
            Container c1 =  new Container(new BoxLayout(BoxLayout.Y_AXIS));
            TextArea  nom = new TextArea (c.getLibelleProjet());
            TextArea cpt = new TextArea();
            TextArea desc = new TextArea(c.getDescProjet());
            TextArea minBdg = new TextArea("Budget: "+c.getBdgMinProjet()+" - "+c.getBdgMaxProjet()+" DT");
            if(c.getCptProjet5()==null){
                if(c.getCptProjet4()==null){
                    if(c.getCptProjet3()==null){
                        if(c.getCptProjet2()==null){
                            if(c.getCptProjet1()==null){
                                }else{cpt.setText(""+c.getCptProjet1()); c1.add(cpt);}
                        }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()); c1.add(cpt);}
                    }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()+" - "+c.getCptProjet3()); c1.add(cpt);}
                }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()+" - "+c.getCptProjet3()+" - "+c.getCptProjet4()); c1.add(cpt);}
            }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()+" - "+c.getCptProjet3()+" - "+c.getCptProjet4()+" - "+c.getCptProjet5()); c1.add(cpt);}
            nom.setEditable(false);
            cpt.setEditable(false);
            minBdg.setEditable(false);
            
            
            Font smallBoldProportionalFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL);
            Font largeBoldProportionalFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
         
            
            c1.getAllStyles().setMarginTop(10);
            c1.getAllStyles().setBorder(Border.createEmpty());
            nom.getAllStyles().setFont(largeBoldProportionalFont);
            desc.getAllStyles().setFont(smallBoldProportionalFont);
            cpt.getAllStyles().setFont(smallBoldProportionalFont);
            minBdg.getAllStyles().setFont(smallBoldProportionalFont);
            
            
            nom.getAllStyles().setBgColor(rgb(156, 91, 87));
            nom.getAllStyles().setFgColor(0xffffffff);
            cpt.getAllStyles().setFgColor(rgb(156, 91, 87));
           
            nom.getAllStyles().setBorder(Border.createEmpty());
            desc.getAllStyles().setBorder(Border.createEmpty());
            cpt.getAllStyles().setBorder(Border.createEmpty());
            minBdg.getAllStyles().setBorder(Border.createEmpty());
            minBdg.getAllStyles().setAlignment(Component.RIGHT);
             
            nom.getAllStyles().setMarginBottom(0);
            desc.getAllStyles().setMarginTop(0);
            desc.getAllStyles().setMarginBottom(0);        
            cpt.getAllStyles().setMarginTop(0);
            cpt.getAllStyles().setMarginBottom(0);
            minBdg.getAllStyles().setMarginTop(0);
            c1.add(nom);
            c1.add(desc);
            c1.add(minBdg);
            TextArea  proDesc = new TextArea ("");
           
            proDesc.getAllStyles().setFont(smallBoldProportionalFont);
            proDesc.getAllStyles().setBorder(Border.createEmpty());
            proDesc.getAllStyles().setMarginTop(2);
            proDesc.getAllStyles().setMarginBottom(2);  
            
           Label Quantite = new Label("Budget : ");
           Label prix = new Label();
                    
                  
                    //Start Slide
                    
                Slider s = new Slider();
                s.setMinValue((int) c.getBdgMinProjet());
                s.setMaxValue((int) c.getBdgMaxProjet());
                s.setEditable(true);
                
                s.addDataChangedListener(new DataChangedListener() {
                    @Override
                    public void dataChanged(int type, int index) {
                        prix.setText(""+s.getProgress());
                    }
                });
           
                
                c1.add(Quantite);
                c1.add(prix);
                c1.add(s);
                c1.add(proDesc);
                f.add(c1);

               Container c2 =  new Container(new BoxLayout(BoxLayout.Y_AXIS));
               c2.getAllStyles().setAlignment(Component.CENTER);
               Button postuler = new Button();
               postuler.setText("                      Postuler                        ");
               postuler.getAllStyles().setBorder(RoundBorder.create().rectangle(true).stroke(new Stroke(2, Stroke.CAP_SQUARE, Stroke.JOIN_MITER, 4)).
               strokeColor(rgb(156, 91, 87)).strokeOpacity(120));
               postuler.getAllStyles().setFont(largeBoldProportionalFont);
               postuler.getAllStyles().setFgColor(0xffffffff);
               postuler.getAllStyles().setBgColor(rgb(156, 91, 87));
               c2.getAllStyles().setBgColor(rgb(156, 91, 87));
               c2.add(postuler);
               f.add(c2);
               
               postuler.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        ServiceProposition SP = new ServiceProposition();
                        Proposition p = new Proposition();
                        p.setIdPMember(6);//set User ID
                        p.setIdPProjet(c.getIdProjet());
                        p.setDescProposition(proDesc.getText());
                        p.setBudgetProposition(Integer.parseInt(prix.getText()));
                        SP.ajoutTask(p);
                    }
               }); 
              f.show();
            
    }
    
}

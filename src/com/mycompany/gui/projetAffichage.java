/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gui;

import static com.codename1.charts.util.ColorUtil.rgb;
import com.codename1.ui.Button;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Font;
import com.codename1.ui.Form;
import com.codename1.ui.Stroke;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.RoundBorder;
import com.mycompany.Entite.Projet;
import com.mycompany.Entite.Proposition;
import com.mycompany.Entite.User;
import com.mycompany.Service.ServiceProjet;
import com.mycompany.Service.ServiceProposition;
import com.mycompany.Service.ServiceUserMob;
import java.util.ArrayList;

/**
 *
 * @author NOUIRA Nesrine
 */
public class projetAffichage {
    
    Form f;
     
     public projetAffichage() {
        f = new Form();
       
    }

    public Form getF() {
        return f;
    }

    public void setF(Form f) {
        this.f = f;
    }
    
    public void projetAff(int id){
        Font smallBoldProportionalFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_SMALL);
        Font largeBoldProportionalFont = Font.createSystemFont(Font.FACE_PROPORTIONAL, Font.STYLE_BOLD, Font.SIZE_MEDIUM);
            
        ServiceProjet service=new ServiceProjet();
        Projet c = service.getProjet1(id);
        f.removeAll();
      Toolbar tb = f.getToolbar();
      tb.removeAll();
    
      
      tb.addCommandToLeftBar("back", null, new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent evt) {
          Affichage h = new Affichage();
          
         }
        });
       Container c1 =  new Container(new BoxLayout(BoxLayout.Y_AXIS));
            TextArea  nom = new TextArea (c.getLibelleProjet());
            TextArea cpt = new TextArea();
            TextArea desc = new TextArea(c.getDescProjet());
            TextArea minBdg = new TextArea("Budget: "+c.getBdgMinProjet()+" - "+c.getBdgMaxProjet()+" DT");
            nom.setEditable(false);
            cpt.setEditable(false);
            minBdg.setEditable(false);
              
            c1.getAllStyles().setMarginTop(10);
            c1.getAllStyles().setBorder(Border.createEmpty());
            nom.getAllStyles().setFont(largeBoldProportionalFont);
            desc.getAllStyles().setFont(smallBoldProportionalFont);
            cpt.getAllStyles().setFont(smallBoldProportionalFont);
            minBdg.getAllStyles().setFont(smallBoldProportionalFont);
            
            
            nom.getAllStyles().setBgColor(rgb(156, 91, 87));
            nom.getAllStyles().setFgColor(0xffffffff);
            cpt.getAllStyles().setFgColor(rgb(156, 91, 87));
           
            nom.getAllStyles().setBorder(Border.createEmpty());
            desc.getAllStyles().setBorder(Border.createEmpty());
            cpt.getAllStyles().setBorder(Border.createEmpty());
            minBdg.getAllStyles().setBorder(Border.createEmpty());
            minBdg.getAllStyles().setAlignment(Component.RIGHT);
             
            nom.getAllStyles().setMarginBottom(0);
            desc.getAllStyles().setMarginTop(0);
            desc.getAllStyles().setMarginBottom(0);        
            cpt.getAllStyles().setMarginTop(0);
            cpt.getAllStyles().setMarginBottom(0);
            minBdg.getAllStyles().setMarginTop(0);
            c1.add(nom);
            c1.add(desc);
             if(c.getCptProjet5()==null){
                if(c.getCptProjet4()==null){
                    if(c.getCptProjet3()==null){
                        if(c.getCptProjet2()==null){
                            if(c.getCptProjet1()==null){
                                }else{cpt.setText(""+c.getCptProjet1()); c1.add(cpt);}
                        }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()); c1.add(cpt);}
                    }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()+" - "+c.getCptProjet3()); c1.add(cpt);}
                }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()+" - "+c.getCptProjet3()+" - "+c.getCptProjet4()); c1.add(cpt);}
            }else{cpt.setText(""+c.getCptProjet1()+" - "+c.getCptProjet2()+" - "+c.getCptProjet3()+" - "+c.getCptProjet4()+" - "+c.getCptProjet5()); c1.add(cpt);}
            c1.add(minBdg);
          
             f.add(c1);
            
           Container c2 =  new Container(new BoxLayout(BoxLayout.Y_AXIS));
           c2.getAllStyles().setAlignment(Component.CENTER);
           Button postuler = new Button();
           postuler.setText("                      Postuler                        ");
           postuler.getAllStyles().setBorder(RoundBorder.create().rectangle(true).stroke(new Stroke(2, Stroke.CAP_SQUARE, Stroke.JOIN_MITER, 4)).
           strokeColor(rgb(156, 91, 87)).strokeOpacity(120));
           postuler.getAllStyles().setFont(largeBoldProportionalFont);
           postuler.getAllStyles().setFgColor(0xffffffff);
           postuler.getAllStyles().setBgColor(rgb(156, 91, 87));
           c2.getAllStyles().setBgColor(rgb(156, 91, 87));
           c2.add(postuler);
           f.add(c2);
           
           postuler.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                Postuler Pp = new Postuler();
                Pp.formulaireAff(c);
             }
            });
           
           
           Container c3 =  new Container(new BoxLayout(BoxLayout.Y_AXIS));
           TextArea  autre = new TextArea ("Propositions :                            ");
           autre.setEditable(false);
           autre.getAllStyles().setBorder(Border.createEmpty());
           c3.add(autre);
           f.add(c3);
          
           ServiceProposition sp =new ServiceProposition();
           ArrayList<Proposition> props = sp.getListeProps(id); 
           
           
           
           
           
           
           if(props != null)
           {
           
           
           for(Proposition p: props){
            Container cp =  new Container(new BoxLayout(BoxLayout.Y_AXIS));
            ServiceUserMob Us = new ServiceUserMob();
            ArrayList<User> listUs = Us.getListUser();
             TextArea  nomp = new TextArea ("");
            for(User u: listUs){
            if(p.getIdPMember()==u.getId()){
             nomp.setText(u.getLogin());
             }
            }
           
            TextArea descp = new TextArea(""+p.getDescProposition());
            TextArea Bdg = new TextArea("Budget : "+p.getBudgetProposition()+" DT");
            nomp.setEditable(false);
            descp.setEditable(false);
            Bdg.setEditable(false);
              
            cp.getAllStyles().setMarginTop(10);
            cp.getAllStyles().setBorder(Border.createEmpty());
            nomp.getAllStyles().setFont(largeBoldProportionalFont);
            descp.getAllStyles().setFont(smallBoldProportionalFont);
            Bdg.getAllStyles().setFont(smallBoldProportionalFont);
            
            
            nomp.getAllStyles().setBgColor(rgb(156, 91, 87));
            nomp.getAllStyles().setFgColor(0xffffffff);
            descp.getAllStyles().setFgColor(rgb(156, 91, 87));
            Bdg.getAllStyles().setFgColor(rgb(156, 91, 87));
            nomp.getAllStyles().setBorder(Border.createEmpty());
            descp.getAllStyles().setBorder(Border.createEmpty());
            Bdg.getAllStyles().setBorder(Border.createEmpty());
            Bdg.getAllStyles().setAlignment(Component.RIGHT);
             
            nomp.getAllStyles().setMarginBottom(0);
            descp.getAllStyles().setMarginTop(0);
            descp.getAllStyles().setMarginBottom(0);
            Bdg.getAllStyles().setMarginTop(0);
            cp.add(nomp);
            cp.add(descp);
            cp.add(Bdg);
            f.add(cp);
           }
           
           
           }
          
        
           
           f.show();
        
        
    }
    
}
